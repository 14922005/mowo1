[_tb_system_call storage=system/_scene1.ks]

[cm  ]
[bg  storage="151351854070879409177_BG560a_800.jpg"  time="1000"  ]
[chara_show  name="エイミー"  time="1000"  wait="true"  left="-158"  top="18"  width="928"  height="671"  reflect="false"  ]
[tb_show_message_window  ]
[delay  speed="100"  ]
[tb_start_text mode=1 ]
#アミ
わたしはアミ、haha。今回目を覚ましたら、なんと勇者になっていたわ、haha。[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="134747124054213113689_BG10a_80.jpg"  ]
[chara_show  name="仲間"  time="1000"  wait="true"  storage="chara/2/e503698a19094a930578686cae5e7b1.png"  width="882"  height="637"  left="432"  top="39"  reflect="false"  ]
[tb_start_text mode=1 ]
#仲間
あんた、なんかエッチな喘ぎ方してない？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
モンスターと戦うのって、結構疲れるんだからね。それよりも、一緒にいる間、何もやっていないくせに文句ばっかり言うなよ！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#仲間
まあ、お前、俺に何かやらせたいのか？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
ええ、お前だって国王の依頼を受けて、一緒に魔王を討つんじゃなかったのか？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#仲間
そうだけどね。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
じゃあ、せめて何かやれよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#仲間
うーん、お前ら勇者って、本当に手間かけるなぁ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
剣を抜いてアミを一突き　[p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[mask_off  time="1000"  effect="fadeOut"  ]
[tb_start_text mode=1 ]
アミの衣服が破れる[p]
経験値+114545[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
ええええええ！！！！！！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#仲間
ほら、もう何も言わないでくれ。俺だってちゃんと仕事したんだから。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
お前、頭おかしいんじゃないの？なんで俺を刺すと経験値が上がるんだよ！？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#仲間
まあ、お前の焦点がそれになるなんてな。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
え、それ以外どうすりゃいいってんの。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#仲間
ふふ、もうすぐわかるさ。[p]
[_tb_end_text]

[chara_hide  name="仲間"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="134747125007113227040_BG10b_80.jpg"  ]
[tb_start_text mode=1 ]
#アミ
とにかく、まずは彼女を町に送り返して装備を買いに行ってもらおう。[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="01castle_03night1_m.jpg"  ]
[tb_start_text mode=1 ]
#アミ
ああ、ここからが魔王の城だろうな[p]

[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="00s.jpg"  ]
[tb_start_text mode=1 ]
#アミ
ふん、は、は[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
どうして物語がますます真面目でなくなっていくんだ？ここは明言できない世界観なのか、早くちゃんとしたことをしないとまずいかもしれない。[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="24086190.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="01s-d.jpg"  ]
[tb_start_text mode=1 ]
#アミ
え？[p]
[_tb_end_text]

[chara_show  name="魔王"  time="1000"  wait="true"  storage="chara/3/7b37dee56bb25d462125da451c376e7.png"  width="796"  height="575"  left="-98"  top="70"  reflect="false"  ]
[tb_start_text mode=1 ]
#魔王
よっ[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
あんた誰よ！？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#魔王
うまく当てられたね、わたしは魔王だよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
そんなこと全然考えてないけど。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#魔王
ふふ、流石は勇者様。マント一つで挑んでくると本当に俺を倒せると思っているのかね？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
これって誰が仕掛けたことなのよ！？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ内心
ちょっと待って、彼のリズムに乗せられてはいけない。会話の主導権を取り戻さないと。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
ふん、こんなダメな魔王相手に、装備なんて要らないって言ってるのよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#魔王
おお、それならお前、そう思っているんだね。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
私の考えを勝手に推測しないでよ、女。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ内心
そうは言っても、さっき一刀で防御を破られたことを考えると、どうやって戦うか考えないと。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
無茶なことはしないほうがいいわ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#魔王
降伏しろ[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
え？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
そんなこと言ってる場合じゃないでしょ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ内心
でも、もし今戻って報告すると…[p]
[_tb_end_text]

[chara_hide  name="エイミー"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_hide  name="魔王"  time="1000"  wait="true"  pos_mode="true"  ]
[bg  time="1000"  method="crossfade"  storage="22832450.jpg"  ]
[tb_start_text mode=1 ]
アミの想像[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#国王
魔王がこんなに早く降伏したのか？それじゃあお前、もう必要ないな。そして、勇者様はあまりにも強すぎるな。これじゃあ都合が悪い。兵たち、勇者を引きずり出して首を斬れ！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="01s-d.jpg"  ]
[tb_start_text mode=1 ]
#アミ内心
うーん、確率は低いけど、あり得ることだし、一応の手続きを踏まないといけない。まだ魔王が何か企んでいる可能性もあるし。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
女、お前、わざと俺を欺いているのか？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#魔王
え？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
敗北後のCGイベントとか考えているのか、あんたもあまりにも積極的じゃない？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#魔王
ああ、お前の妄想をやめてくれ。それにしても、今、降伏してもいいのか？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
あんた魔王でしょ、どうしてそんなに降伏したがってるの？それに、手に持ってるのは何？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#魔王
お前のものだよ。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#アミ
死ね！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="154650782217978432178_BG600a_800.jpg"  ]
[tb_start_text mode=1 ]
#ナレーション
勇者はついに魔王を打ち倒した（？）。おめでとう、おめでとう！[p]
[_tb_end_text]

[tb_image_hide  time="1000"  ]
[s  ]
